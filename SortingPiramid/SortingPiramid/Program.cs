﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace SortingPiramid
{
    class PyramidSorting
    {
        //добавление элемента к пирамиде
        static int add2pyramid(double[] arr, int i, int N)
        {
            int imax;
            double buf;
            if ((2 * i + 2) < N)
            {
                if (arr[2 * i + 1] < arr[2 * i + 2]) imax = 2 * i + 2;
                else imax = 2 * i + 1;
            }
            else imax = 2 * i + 1;
            if (imax >= N) return i;
            if (arr[i] < arr[imax])
            {
                buf = arr[i];
                arr[i] = arr[imax];
                arr[imax] = buf;
                if (imax < N / 2) i = imax;
            }
            return i;
        }
        public static void sorting(double[] arr, int len)
        {
            //шаг 1: постройка пирамиды
            for (int i = len / 2 - 1; i >= 0; --i)
            {
                long prev_i = i;
                i = add2pyramid(arr, i, len);
                if (prev_i != i) ++i;
                Test.count++;
            }
   
            //шаг 2: сортировка
            double buf;
            for (int k = len - 1; k > 0; --k)
            {
                buf = arr[0];
                arr[0] = arr[k];
                arr[k] = buf;
                int i = 0, prev_i = -1;
                while (i != prev_i)
                {
                    Test.count++;
                    prev_i = i;
                    i = add2pyramid(arr, i, k);
                }
            }
        }
    }
    class Test
    {
        public static int count = 0;
        static void Main(string[] args)
        {
            
            try
            {
                using (StreamReader sr = new StreamReader("test.txt"))
                {
                    double[] arr;
                    int a = 1;
                    while(true)
                    {
                        Stopwatch stopWatch = new Stopwatch();
                        stopWatch.Start();
                        String line = sr.ReadLine();
                        if (line == null) break;
                        arr = line.Split(' ').Select(x => Double.Parse(x)).ToArray();
                        PyramidSorting.sorting(arr, arr.Length);
                        stopWatch.Stop();
                        TimeSpan ts = stopWatch.Elapsed;
                        //Console.WriteLine(a + ". " + String.Join(", ",arr.Select( x => x.ToString())));
                        Console.Write("RunTime " + ts.TotalMilliseconds);
                        Console.Write(", number of iterations " + count);
                        Console.WriteLine();
                        count = 0;
                        a++;
                    }
                   
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            
            
        }
    }
}