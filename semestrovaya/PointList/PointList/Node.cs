﻿using System.Drawing;
using System.Reflection.Metadata.Ecma335;

namespace PointList
{
    public class Node
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public int x { get; set; }

        public int y{ get; set; }
        public int x1{ get; set; }
        public int y1{ get; set; }       
        public Node(int x,int y,int x1,int y1)
        {
            this.x = x;
            this.y = y;
            this.x1 = x1;
            this.y1 = y1;
        }
        /// <summary>
        /// Ссылка на следующий элемент
        /// </summary>
        public Node NextNode { get; set; }

        
    }
    
}