﻿using System;
using System.Globalization;

namespace PointList
{
    class Program
    {
        static void Main(string[] args)
        {
            SegmentList a = new SegmentList("txt");
            a.DictionaryFeeling();
            var fa = new Node(30,30,0,0);
            var va = new Node(30,32,0,0);
            Console.WriteLine("SegmentList а ниже ");
            a.Show();
            a.Insert(fa);
            a.Insert(fa);
            a.Insert(va);
            a.Insert(va);
            Console.WriteLine("Вставили два Node (30,30,0,0) и (30,32,0,0)");
            a.Show();
            Console.WriteLine();
            Console.WriteLine("Создали список состоящий из отрезков, которые наклонены к оси абсцисс под углами 30 и 45 градусов");
            a.AngleList();
            a.Show();
            Console.WriteLine("Создали список который находится между a и b");
            a.lenghtList(1, 40);
            a.Show();
            a.sort();
            Console.WriteLine("Отсортировали");
            a.Show();
        }
    }
}