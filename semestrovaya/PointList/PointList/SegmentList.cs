﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace PointList
{
    public class SegmentList
    {
        public Node _head;
        public Dictionary<int,string> dictionary = new Dictionary<int,string>();
        public SegmentList(string a)
        {
            try{
                using (StreamReader sr = new StreamReader(@"C:\Users\Frameworker\taziev_11910\semestrovaya\PointList\PointList\" + a + ".txt"))
                {
                    string ad = sr.ReadToEnd();
                    string[] numb;
                    numb = ad.Split(' ');
                    if(numb.Length % 4 != 0)throw new Exception("Недостаточно точек у отрезка, отрезок имеет 4 переменные");
                    for (int i = 0; i < numb.Length; i+=4)
                    {
                        if (_head == null)
                        {
                            _head = new Node(Int32.Parse(numb[i]),Int32.Parse(numb[i + 1]),Int32.Parse(numb[i + 2]),Int32.Parse(numb[i + 3]));
                        }
                        else
                        {
                            var node = _head;
                            while (node.NextNode != null)
                                node = node.NextNode;
                            node.NextNode = new Node(Int32.Parse(numb[i]), Int32.Parse(numb[i + 1]), Int32.Parse(numb[i + 2]), Int32.Parse(numb[i + 3]));
                            node = node.NextNode;
                        }
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
        }
        public void showSegment(Node a)
        {
            Console.Write("coordinates of the first vertex - (" + a.x + ", " + a.y + ") coordinates of the second vertex - ("  + a.x1 + ", " + a.y1 + ")");
        }

        public void Show()
        {
            if (_head == null)
            {
                Console.WriteLine("It's empty");
                return;
            }
            var node = _head;
            int count = 1;
            while (node != null)
            {
                Console.Write(count + " - ");
                showSegment(node);
                Console.WriteLine();
                count++;
                node = node.NextNode;
            }
        }

        public int Size()
        {
            int count = 1;
            if (_head == null) return 0;
            var node = _head;
            while (node.NextNode != null)
            {
                node = node.NextNode;
                count++;
            }

            return count;

        }

        public bool CheckAngle(Node a)
        {
            double radians = Math.Atan2(a.y1 - a.y, a.x1 - a.x)*180/Math.PI;
            if (radians == 30 || radians == 45 || radians == -135 || radians == -150)
            {
                return true;
            }
            else return false;
        }

        public SegmentList lenghtList(int a, int b)
        {
            try
            {
                using (StreamReader sr =
                    new StreamReader(@"C:\Users\Frameworker\taziev_11910\semestrovaya\PointList\PointList\txt.txt"))
                {
                    _head = null;
                    string ad = sr.ReadToEnd();
                    string[] numb;
                    numb = ad.Split(' ');
                    if (numb.Length % 4 != 0)
                        throw new Exception("Недостаточно точек у отрезка, отрезок имеет 4 переменные");
                    for (int i = 0; i < numb.Length; i += 4)
                    {
                        if (a <= Math.Sqrt(Math.Pow((Int32.Parse(numb[i]) - Int32.Parse(numb[i + 2])), 2) +
                                           Math.Pow(Int32.Parse(numb[i + 1]) - Int32.Parse(numb[i + 3]), 2)))
                        {
                            if (b >= Math.Sqrt(Math.Pow((Int32.Parse(numb[i]) - Int32.Parse(numb[i + 2])), 2) +
                                               Math.Pow(Int32.Parse(numb[i + 1]) - Int32.Parse(numb[i + 3]), 2)))
                            {
                                if (_head == null)
                                {
                                    _head = new Node(Int32.Parse(numb[i]), Int32.Parse(numb[i + 1]),
                                        Int32.Parse(numb[i + 2]), Int32.Parse(numb[i + 3]));
                                }
                                else
                                {
                                    var node = _head;
                                    while (node.NextNode != null)
                                        node = node.NextNode;
                                    node.NextNode = new Node(Int32.Parse(numb[i]), Int32.Parse(numb[i + 1]),
                                        Int32.Parse(numb[i + 2]), Int32.Parse(numb[i + 3]));
                                    node = node.NextNode;
                                }
                            }
                        }
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }
            finally
            {
                
            }
            return this;
        }

        public SegmentList AngleList()
        {
            try{
                using (StreamReader sr = new StreamReader(@"C:\Users\Frameworker\taziev_11910\semestrovaya\PointList\PointList\txt.txt"))
                {
                    _head = null;
                    string ad = sr.ReadToEnd();
                    string[] numb;
                    numb = ad.Split(' ');
                    if(numb.Length % 4 != 0)throw new Exception("Недостаточно точек у отрезка, отрезок имеет 4 переменные");
                    for (int i = 0; i < numb.Length; i+=4)
                    {
                        if (CheckAngle(new Node(Int32.Parse(numb[i]), Int32.Parse(numb[i + 1]),
                            Int32.Parse(numb[i + 2]), Int32.Parse(numb[i + 3]))))
                        {
                            if (_head == null)
                            {
                                _head = new Node(Int32.Parse(numb[i]), Int32.Parse(numb[i + 1]),
                                    Int32.Parse(numb[i + 2]), Int32.Parse(numb[i + 3]));
                            }
                            else
                            {
                                var node = _head;
                                while (node.NextNode != null)
                                    node = node.NextNode;
                                node.NextNode = new Node(Int32.Parse(numb[i]), Int32.Parse(numb[i + 1]),
                                    Int32.Parse(numb[i + 2]), Int32.Parse(numb[i + 3]));
                                node = node.NextNode;
                            }
                        }
                    }
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            return this;
        }

        public void DictionaryFeeling()
        {
            if (_head == null)
            {
                return;
            }

            var node = _head;
            int i = 0;
            while (node.NextNode != null)
            {
                dictionary.Add(i,String(node));
                node = node.NextNode;
                i++;
            }
            
        }

        public bool CheckUnikum(Node a)
        {
            return !dictionary.ContainsValue(String(a));
        }
        
        public void Insert(Node f)
        {
            if (CheckUnikum(f))
            {
                var node = _head;
                while (node.NextNode != null)
                {
                    node = node.NextNode;
                }

                node.NextNode = f;
            }
            else
            {
                return;
            }
        }

        public string String(Node a)
        {
            return a.x + " " + a.y + " " + a.x1 + " " + a.y1;
        }

        private double Size(Node a)
        {
            double size = Math.Sqrt((a.x1 - a.x) * (a.x1 - a.x) + (a.y1 - a.y) * (a.y1 - a.y));
            return size;
        }
        static void Swap( Node a, Node b)
        {
            var swit = 0;
            swit = a.x;
            a.x = b.x;
            b.x = swit;
            
            swit = a.x1;
            a.x1 = b.x1;
            b.x1= swit;
            
            swit = a.y;
            a.y = b.y;
            b.y = swit;
            
            swit = a.y1;
            a.y1 = b.y1;
            b.y1 = swit;
        }

        public void sort()
        {
            if (_head == null)
            {
                return;
            }

            
            int count = -1;
            while (count < 0)
            {
                var firstnode = _head;
                var secondnode = _head.NextNode;
                count = 0;
                while (secondnode != null)
                {
                    if (Size(firstnode) > Size(secondnode))
                    {
                        Swap(firstnode,secondnode);
                        count--;
                    }
                    secondnode = secondnode.NextNode;
                    firstnode = firstnode.NextNode;
                }
            }
        }
    }
}