﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// Параметры выхода в отпуск
    /// </summary>
    public class GoVacationParams
    {
        /// <summary>
        /// Дата начала выхода
        /// </summary>
        public DateTime DateFrom { get; set; }

        /// <summary>
        /// Дата окончания отпуска
        /// </summary>
        public DateTime DateTo { get; set; }

        /// <summary>
        /// Административный отпуск
        /// </summary>
        public bool IsAdmVacation { get; set; }
    }
}
