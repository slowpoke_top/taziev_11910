﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// Работник
    /// </summary>
    public class Worker
    {
        /// <summary>
        /// Экземпляр делегата, через который мы будем обрабатывать событие 
        /// </summary>
        public Action<Worker, GoVacationParams> eventDelegate;

        /// <summary>
        /// Идет в отпуск
        /// </summary>
        public void GoVacation(GoVacationParams param) 
        {
            if (eventDelegate != null)
                eventDelegate(this, param);
            //m.FindReplacement(this);
            //fd.CalcVacationPay(this);
        }
    }
}
