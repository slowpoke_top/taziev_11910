﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// Управленец уведомлениями
    /// </summary>
    public class MailManager
    {
        /// <summary>
        /// Новое уведомление
        /// </summary>
        public event EventHandler<NewMailEventArgs> NewMail;

        protected void OnNewMail(NewMailEventArgs args) 
        {
            if (NewMail != null)
                NewMail(this, args);
        }

        // информацию в желаемое событие
        public void SimulateNewMail(String from, String to, String subject)
        {
            // Создать объект для хранения информации, которую
            // нужно передать получателям уведомления
            var e = new NewMailEventArgs(from, to, subject);
            // Вызвать виртуальный метод, уведомляющий объект о событии
            // Если ни один из производных типов не переопределяет этот метод,
            // объект уведомит всех зарегистрированных получателей уведомления
            OnNewMail(e);
        }
    }
}
