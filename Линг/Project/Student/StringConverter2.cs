﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// Добавление суффикса
    /// </summary>
    public class StringConverter2 : IConverter<string>
    {
        public string Convert(string arg)
        {
            return arg + " Рената";
        }
    }
}
