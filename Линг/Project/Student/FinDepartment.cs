﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// Фин отдел компании
    /// </summary>
    public class FinDepartment
    {
        public void Register(Worker worker) 
        {
            worker.eventDelegate += CalcVacationPay;
        }

        public void UnRegister(Worker worker)
        {
            worker.eventDelegate -= CalcVacationPay;
        }

        public void CalcVacationPay(Worker worker, GoVacationParams param) 
        {
            Console.WriteLine("Я фин отдел, считаю отпускные сотрудника " + 
                $" с {param.DateFrom.ToShortDateString()}" +
                $" по {param.DateTo.ToShortDateString()}");
        }
    }
}
