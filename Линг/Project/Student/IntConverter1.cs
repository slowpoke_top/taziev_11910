﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// умножение на 2
    /// </summary>
    public class IntConverter1 : IConverter<int>
    {
        public int Convert(int arg)
        {
            return arg * arg;
        }
    }
}
