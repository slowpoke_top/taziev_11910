﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    public delegate int SingleMathOperation(int x);

    public class DelegateExamples
    {
        public int PowerTwo(int x) 
        {
            return x * x;
        }

        public int ModSeven(int x) 
        {
            return x % 7;
        }

        public double DivisionTwo(int x) 
        {
            return x / 2;
        }

        public void Run() 
        {
            //создание экземпляра
            SingleMathOperation d1;
            d1 = PowerTwo;
            var d11 = new SingleMathOperation(ModSeven);

            var a = 5;
            a = d1(a);
            a = d11.Invoke(a);

            d1 = null;

            d11 += PowerTwo;
            d11 += PowerTwo;

            a = d11(a);

            //минус три анонимный метод
            SingleMathOperation d2 = delegate (int x) { return x - 3; };
            // минус три лямбда-выражение
            SingleMathOperation d22 = (int x) => { return x - 3; };
            d22 += (int x) => x - 3;

            var d3 = new Action<string, string>(
                (string s1, string s2) => Console.WriteLine(s1 + s2));

            var d4 = new Func<int, int>(PowerTwo);
        }

    }
}
