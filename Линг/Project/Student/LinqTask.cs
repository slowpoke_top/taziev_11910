﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Student
{
    /// <summary>
    /// Задание по LINQ
    /// </summary>
    public class LinqTask
    {
        public void Run() {
            var houses = new List<House>
            {
                new House{ Id = 1, Number = 1, Building = "А"},
                new House{ Id = 2, Number = 1, Building = "Б"},
                new House{ Id = 3, Number = 2},
            };
            var apartments = new List<Apartment>
            {
                new Apartment{ Id = 1, HouseId = 1, Number = 1 },
                new Apartment{ Id = 2, HouseId = 2, Number = 1 },
                new Apartment{ Id = 3, HouseId = 3, Number = 1 },
                new Apartment{ Id = 4, HouseId = 3, Number = 2 },
                new Apartment{ Id = 5, HouseId = 3, Number = 3 },
                new Apartment{ Id = 6, HouseId = 3, Number = 4 },
                new Apartment{ Id = 7, HouseId = 3, Number = 5 }
            };
            var services = new List<Service>
            {
                new Service{Id =  6, Name = "Холодная вода"},
                new Service{Id =  7, Name = "Канализация"},
                new Service{Id =  8, Name = "Отопление"},
                new Service{Id =  9, Name = "Подогрев воды"},
                new Service{Id =  10, Name = "Газ"},
                new Service{Id =  14, Name = "Холодная вода для нужд ГВС"},
                new Service{Id =  16, Name = "Вывод ТБО"},
                new Service{Id =  24, Name = "вывоз ЖБО"}
            };
            var accruals = new List<Accruals>
            {
                new Accruals{ Id = 1, ApartmentId = 1, ServiceId = 6, InBalance = 100, SumAccrued = 456, SumMoney = 24},
                new Accruals{ Id = 2, ApartmentId = 1, ServiceId = 7, InBalance = 46, SumAccrued = 23, SumMoney = 62},
                new Accruals{ Id = 3, ApartmentId = 1, ServiceId = 10, InBalance = 56, SumAccrued = 456, SumMoney = 756},
                new Accruals{ Id = 4, ApartmentId = 1, ServiceId = 16, InBalance = 35, SumAccrued = 34, SumMoney = 345},
                new Accruals{ Id = 5, ApartmentId = 1, ServiceId = 24, InBalance = 656, SumAccrued = 23, SumMoney = 34},
                 
                new Accruals{ Id = 6, ApartmentId = 2, ServiceId = 6, InBalance = -23, SumAccrued = 75, SumMoney = 56},
                new Accruals{ Id = 7, ApartmentId = 2, ServiceId = 7, InBalance = -354, SumAccrued = 46, SumMoney = 34},
                new Accruals{ Id = 8, ApartmentId = 2, ServiceId = 10, InBalance = -46, SumAccrued = 45, SumMoney = 65},
                new Accruals{ Id = 9, ApartmentId = 2, ServiceId = 16, InBalance = -24, SumAccrued = 76, SumMoney = 75},
                new Accruals{ Id = 10, ApartmentId = 2, ServiceId = 24, InBalance = -98, SumAccrued = 788, SumMoney = 876},

                new Accruals{ Id = 11, ApartmentId = 3, ServiceId = 6, InBalance = 567, SumAccrued = 78, SumMoney = 34},
                new Accruals{ Id = 12, ApartmentId = 3, ServiceId = 7, InBalance = -78, SumAccrued = 85, SumMoney = 243},
                new Accruals{ Id = 13, ApartmentId = 3, ServiceId = 9, InBalance = 56, SumAccrued = 50, SumMoney = 254},
                new Accruals{ Id = 14, ApartmentId = 3, ServiceId = 10, InBalance = -74, SumAccrued = 78, SumMoney = 150},
                new Accruals{ Id = 15, ApartmentId = 3, ServiceId = 14, InBalance = 54, SumAccrued = 60, SumMoney = 250},
                new Accruals{ Id = 16, ApartmentId = 3, ServiceId = 16, InBalance = -85, SumAccrued = 78, SumMoney = 345},
                new Accruals{ Id = 17, ApartmentId = 3, ServiceId = 24, InBalance = 47, SumAccrued = 57, SumMoney = 54},

                new Accruals{ Id = 18, ApartmentId = 4, ServiceId = 6, InBalance = 68, SumAccrued = 68, SumMoney = 78},
                new Accruals{ Id = 19, ApartmentId = 4, ServiceId = 7, InBalance = -56, SumAccrued = 78, SumMoney = 68},
                new Accruals{ Id = 20, ApartmentId = 4, ServiceId = 9, InBalance = 85, SumAccrued = 68, SumMoney = 78},
                new Accruals{ Id = 21, ApartmentId = 4, ServiceId = 10, InBalance = 45, SumAccrued = 45, SumMoney = 68},
                new Accruals{ Id = 22, ApartmentId = 4, ServiceId = 14, InBalance = -85, SumAccrued = 37, SumMoney = 45},
                new Accruals{ Id = 23, ApartmentId = 4, ServiceId = 16, InBalance = 48, SumAccrued = 78, SumMoney = 85},
                new Accruals{ Id = 24, ApartmentId = 4, ServiceId = 24, InBalance = 85, SumAccrued = 50, SumMoney = 714},

                new Accruals{ Id = 25, ApartmentId = 5, ServiceId = 6, InBalance = 345, SumAccrued = 54, SumMoney = 785},
                new Accruals{ Id = 26, ApartmentId = 5, ServiceId = 7, InBalance = 54, SumAccrued = 45, SumMoney = 78},
                new Accruals{ Id = 27, ApartmentId = 5, ServiceId = 10, InBalance = -23, SumAccrued = 34, SumMoney = 58},
                new Accruals{ Id = 28, ApartmentId = 5, ServiceId = 16, InBalance = 65, SumAccrued = 546, SumMoney = 785},
                new Accruals{ Id = 29, ApartmentId = 5, ServiceId = 24, InBalance = 23, SumAccrued = 34, SumMoney = 75},

                new Accruals{ Id = 30, ApartmentId = 6, ServiceId = 6, InBalance = 62, SumAccrued = 45, SumMoney = 67},
                new Accruals{ Id = 31, ApartmentId = 6, ServiceId = 7, InBalance = -345, SumAccrued = 31, SumMoney = 48},
                new Accruals{ Id = 32, ApartmentId = 6, ServiceId = 10, InBalance = 23, SumAccrued = 30, SumMoney = 256},
                new Accruals{ Id = 33, ApartmentId = 6, ServiceId = 16, InBalance = 453, SumAccrued = 50, SumMoney = 545},
                new Accruals{ Id = 34, ApartmentId = 6, ServiceId = 24, InBalance = 234, SumAccrued = 20, SumMoney = 34},

                new Accruals{ Id = 35, ApartmentId = 7, ServiceId = 6, InBalance = 245, SumAccrued = 345, SumMoney = 150},
                new Accruals{ Id = 36, ApartmentId = 7, ServiceId = 7, InBalance = 64, SumAccrued = 56, SumMoney = 456},
                new Accruals{ Id = 37, ApartmentId = 7, ServiceId = 10, InBalance = 456, SumAccrued = 34, SumMoney = 34},
                new Accruals{ Id = 38, ApartmentId = 7, ServiceId = 16, InBalance = -34, SumAccrued = 50, SumMoney = 160},
                new Accruals{ Id = 39, ApartmentId = 7, ServiceId = 24, InBalance = 576, SumAccrued = 62, SumMoney = 100}
            };

            //Задания
            
            //1. Вывести по каждому ЛС полный адрес: номер дома, номер квартиры
            var collectionID = apartments.Join(houses,
                a => a.HouseId,
                h => h.Id,
                (a, h) => new
                {
                    AppartmentId = a.Id,
                    Address = "дом " + h.Number + (!String.IsNullOrEmpty(h.Building) ? " корпус " + h.Building : "") +
                              " квартира " + a.Number
                });
            //2. Для каждого дома вывести список оказываемых услуг (наименования)
            var res2 = houses.Join
            (
                apartments,
                i => i.Id,
                s => s.HouseId,
                (i, s) => accruals.Join
                (
                    services,
                    a => a.ServiceId,
                    b=> b.Id,
                    (a,b)=> (a.ApartmentId == s.Id ? "HouseId " + i.Id + " Service Name " + b.Name : "")
                )
            ) ;
            foreach(var a in res2)
            {
                foreach(var b in a)
                {
                    if(b!="")
                        Console.WriteLine(b);
                }
            }
            //3. Написать функцию, которой на вход передается номер квартиры,
            //а на выходе получаем счет-фактуру: полный адрес дома, 
            //по каждой оказываемой услуге наименование услуги, входящий остаток,
            //начислено, оплачено, 
            //исходящий остаток = входящий остаток + начислено - оплачено,
            //вывести итого для каждого столбца с суммами
            var apart = Convert.ToInt32(Console.ReadLine());
            var collectionPay = accruals.Where(x => x.ApartmentId == apart).Select(i => new
            {
                i.Id,
                i.ApartmentId,
                i.ServiceId,
                i.SumAccrued,
                i.SumMoney,
                i.InBalance,
                outBalance = i.InBalance - i.SumMoney + i.SumAccrued
            });
            var sumresult = new[] { collectionPay.Sum(x => x.InBalance), collectionPay.Sum(x => x.SumMoney), collectionPay.Sum(x => x.SumAccrued), collectionPay.Sum(x => x.outBalance)};
            Console.WriteLine(collectionID.Where(x => x.AppartmentId == apart).Select(x => x.Address).FirstOrDefault());
            var reusltPay = collectionPay.Join(services, i => i.ServiceId, j => j.Id, (i, j) => j.Name + " Входящий остаток " + i.InBalance + "Оплачено" + i.SumMoney
                                                                                                + "Начислено" + i.SumAccrued + "Исходящий остаток" + i.outBalance);
            foreach (var item in reusltPay)
            {
                Console.WriteLine(item);
            }
            Console.Write("Итого: ");
            foreach (var item in sumresult)
            {
                Console.Write(item + " ");
            }
            //4. Вывести для каждого дома список квартир-должников с суммой долга 
            //(исходяйщий остаток из задания 3 со знаком +), 
            //отдельно укзаать должника с наибольшей суммой долга
        }
    }

    /// <summary>
    /// Дом
    /// </summary>
    class House 
    {
        /// <summary>
        /// Идентификатор дома
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Номер дома
        /// </summary>
        public int Number { get; set; }
        /// <summary>
        /// Корпус
        /// </summary>
        public string Building { get; set; }
    }

    /// <summary>
    /// Квартира
    /// </summary>
    class Apartment
    {
        /// <summary>
        /// Идентификатор квартиры
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Идентификатор дома
        /// </summary>
        public int HouseId { get; set; }
        /// <summary>
        /// Номер квартиры
        /// </summary>
        public int Number { get; set; }
    }

    /// <summary>
    /// Услуга
    /// </summary>
    class Service
    {
        /// <summary>
        /// Идентификатор услуги
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Наименование услуги
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// Начисления
    /// </summary>
    class Accruals
    {
        /// <summary>
        /// Идентификатор начисления
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Номер квартиры (будем считать, что квартира = лицевой счет, хотя это не так)
        /// </summary>
        public int ApartmentId { get; set; }
        /// <summary>
        /// Идентификатор услуги
        /// </summary>
        public int ServiceId { get; set; }
        /// <summary>
        /// Входящий остаток с прошлого месяца (если с +, то это долг, если с -, то переплата)
        /// </summary>
        public float InBalance { get; set; }
        /// <summary>
        /// Оплата в этом месяце
        /// </summary>
        public float SumMoney { get; set; }
        /// <summary>
        /// Начислено за месяц
        /// </summary>
        public float SumAccrued { get; set; }
    }

}
