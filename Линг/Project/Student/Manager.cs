﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    /// <summary>
    /// Руководитель
    /// </summary>
    public class Manager
    {
        /// <summary>
        /// Подписка на событие "уход сотрудника в отпуск"
        /// </summary>
        public void Register(Worker worker) 
        {
            worker.eventDelegate += FindReplacement;
        }

        public void UnRegister(Worker worker)
        {
            worker.eventDelegate -= FindReplacement;
        }

        /// <summary>
        /// Поиск замены сотрудника
        /// </summary>
        public void FindReplacement(Worker worker, GoVacationParams param) 
        {
            Console.WriteLine("Я руководитель, мой работник уходит в отпуск, " +
                $" я ищу ему замену с {param.DateFrom.ToShortDateString()} " +
                $" по {param.DateTo.ToShortDateString()}");
        }
    }
}
