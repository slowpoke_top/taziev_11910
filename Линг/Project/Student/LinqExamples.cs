﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Student
{
    public class LinqExamples
    {
        private class Student2
        {
            public string First { get; set; }
            public string Last { get; set; }
            public int ID { get; set; }
            public List<int> Scores;
        }

        private class StudentFirstLast
        {
            public string First { get; set; }
            public string Last { get; set; }
        }

        public void Run()
        {
            var students = new List<Student2>()
            {
                new Student2 {First="Svetlana", Last="Omelchenko", ID=111, Scores= new List<int> {97, 92, 81, 60}},
                new Student2 {First="Claire", Last="O'Donnell", ID=112, Scores= new List<int> {75, 84, 91, 39}},
                new Student2 {First="Sven", Last="Mortensen", ID=113, Scores= new List<int> {88, 94, 65, 91}},
                new Student2 {First="Cesar", Last="Garcia", ID=114, Scores= new List<int> {97, 89, 85, 82}},
                new Student2 {First="Debra", Last="Garcia", ID=115, Scores= new List<int> {35, 72, 91, 70}},
                new Student2 {First="Fadi", Last="Fakhouri", ID=116, Scores= new List<int> {99, 86, 90, 94}},
                new Student2 {First="Hanying", Last="Feng", ID=117, Scores= new List<int> {93, 92, 80, 87}},
                new Student2 {First="Hugo", Last="Garcia", ID=118, Scores= new List<int> {92, 90, 83, 78}},
                new Student2 {First="Lance", Last="Tucker", ID=119, Scores= new List<int> {68, 79, 88, 92}},
                new Student2 {First="Terry", Last="Adams", ID=120, Scores= new List<int> {99, 82, 81, 79}},
                new Student2 {First="Eugene", Last="Zabokritski", ID=121, Scores= new List<int> {96, 85, 91, 60}},
                new Student2 {First="Michael", Last="Tucker", ID=122, Scores= new List<int> {94, 92, 91, 91}}
            };

            //Отфильтруем студентов, у которых у имени есть буква "a"
            //способ 1 - без LINQ
            var res1 = new List<Student2>();
            foreach (var s in students)
            {
                if (s.First.Contains("a"))
                    res1.Add(s);
            }
            //способ 2 - синтаксис выражений запросов
            var res2 = (from s in students
                        where s.First.Contains("a")
                        select s).ToList();
            //способ 3 - текучий синтаксис linq, или цепочка операций запросов
            var res3 = students.Where(s => s.First.Contains("a")).ToList();

            //Select
            //тождественная выборка
            var res4 = students.Select(x => x);
            //выборка только 1 поля - все фамилии
            var res5 = students.Select(x => x.Last);
            //выборка списка имен и фамилий через класс StudentFirstLast
            var res6 = students
                .Select(x => new StudentFirstLast()
                {
                    First = x.First,
                    Last = x.Last
                }).ToList();
            //выборка списка имен и фамилий через анонимный класс
            var res7 = students
                .Select(x => new
                {
                    First = x.First,
                    Last = x.Last
                }).ToList();
            //преобразование полей при операции select
            var res8 = students
                .Select(x => new
                {
                    First = String.IsNullOrEmpty(x.First) ? "" : x.First.Substring(0, 1).ToUpper(),
                    Last = x.Last.ToUpper()
                });

            //where - фильтрация данных
            var res9 = students.Where(x =>
            {
                if (x.Scores[0] > 70) return x.First.Contains("a");
                else return true;
            }).ToList();

            //сортировка
            //OrderBy, OrderByDescending, ThenBy, ThenByDescending
            var res10 = students
                .OrderBy(x => x.Last)
                .ThenByDescending(x => x.ID)
                .ToList();


            //-----------------------------------------------------------
            //новые примеры

            //LINQ включает в себя около 50 стандартых операций запросов, 
            //разделяемых на 2 большие группы - 
            //отложенные операции (выполняются не во время инициализации, 
            //а только при их вызове) и не отложенные операции (выполняются сразу). 
            // подробнее: https://professorweb.ru/my/LINQ/base/level1/1_1.php

            //Take, TakeWhile, Skip - для обрезания количесва записей, 
            //может использоваться при paging-е при выборе инфы для интерфейса

            //Take - Возвращает указанное кол-во элементов последовательности с начала
            var r1 = students
                .OrderBy(x => x.Last);
            var r2 = r1.ThenBy(x => x.First);
            var r3 = r2.Take(5);

            var res11 = students
                .OrderBy(x => x.Last)
                .ThenBy(x => x.First)
                .Take(5);
            //а если будет больше, чем элементов в последовательности
            var res12 = students
               .OrderBy(x => x.Last)
               .ThenBy(x => x.First)
               .Take(155);

            //TakeWhile - возвращает элементы из входной последовательности, 
            //пока истинно некоторое условие, начиная с начала последовательности
            var res13 = students
                .OrderBy(x => x.Last)
                .ThenBy(x => x.First)
                .TakeWhile(x => x.Last.Length < 5);

            // Skip пропускает указанное количество элементов из входной последовательности,
            // начиная с ее начала, и выводит остальные
            // Допустим хотим вывести информацию для 2 страницы 
            // (то есть пропускаем 5 для первой страницы и выбираем 5 из оставшихся для второй)
            var res14 = students
                .OrderBy(x => x.Last)
                .ThenBy(x => x.First)
                .Skip(5)
                .Take(5);

            // SkipWhile обрабатывает входную последовательность, пропуская элементы до тех пор, 
            // пока условие истинно, 
            // а затем выводит остальные в выходную последовательность
            var res15 = students
                .OrderBy(x => x.Last)
                .ThenBy(x => x.First)
                .SkipWhile(x => x.Last.Length < 5);

            //операции множеств Distinct, Union, Except и Intersect

            //Distinct удаляет дублированные элементы из входной последовательности
            var res16 = students
                .Select(x => x.Last)
                .Distinct();

            //Union возвращает объединение множеств из двух исходных последовательностей
            //без повторений!
            var res17 = (new List<int> { 1, 2, 3, 4, 5 })
                .Union(new List<int> { 4, 5, 6, 7, 8 });

            //Если хотим с повторениями, то можно использовать Concat
            //Concat соединяет две входные последовательности 
            //и выдает одну выходную последовательность
            var res18 = (new List<int> { 1, 2, 3, 4, 5 })
                .Concat(new List<int> { 4, 5, 6, 7, 8 });

            //Intersect - пересечение множеств из двух исходных последовательностей
            var res19 = (new List<int> { 1, 2, 3, 4, 5 })
                .Intersect(new List<int> { 4, 5, 6, 7, 8 });

            //Except возвращает последовательность, содержащую все элементы первой 
            //последовательности, которых нет во второй последовательности 
            var res20 = (new List<int> { 1, 2, 3, 4, 5 })
                .Except(new List<int> { 4, 5, 6, 7, 8 });

            //Cast попытается привести каждый элемент входной 
            //последовательности к указанному типу
            //Если любой из этих элементов не может быть приведен к указанному типу, 
            //будет сгенерировано исключение InvalidCastException
            //TODO
            var res21 = new List<int> { 1, 2, 3, 4 }.Cast<long>();

            //Если существует вероятность присутствия разнотипных элементов в исходной 
            //коллекции, применяйте вместо Cast операцию OfType
            //OfType используется для построения выходной последовательности, содержащей 
            //только те элементы, которые могут быть успешно преобразованы к указанному типу
            //TODO
            var res22 =
                new List<string> { "1", "2", "true", "Renata", "3", "4" }.OfType<int>();

            //AsEnumerable  позволяет привести входную коллекцию к нормальному типу последовательности 
            //IEnumerable<T>, позволяя вызывать на ней методы стандартных операций запросов. 
            //Пример:  LINQ to SQL работает с последовательностями типа 
            //IQueryable<T>  и реализует собственные операции
            //если мы точно хотим работать с IEnumerable<T>, то AsEnumerable в помощь

            //Группировка GroupBy 
            //Операции группирования помогают объединять вместе 
            //элементы последовательности по общему ключу
            var res23 = students
                .GroupBy(x => x.Last);
            //результат - последовательность IGrouping<K, Т>, где K - тип ключа

            //Join выполняет внутреннее соединение по эквивалентности двух 
            //последовательностей на основе ключей, извлеченных из каждого 
            //элемента этих последовательностей
            var services = new List<Service1>
            {
                new Service1 { Id = 1, Name = "Замена масла" },
                new Service1 { Id = 2, Name = "Замена колес" },
                new Service1 { Id = 3, Name = "Замена фар" }
            };

            var invoces = new List<Invoice>
            {
                new Invoice { ServiceId = 1, Sum = 40 },
                new Invoice { ServiceId = 2, Sum = 7 },
                new Invoice { ServiceId = 1, Sum = 48 },
                new Invoice { ServiceId = 3, Sum = 56 }
            };
            //Задача вывести пары наименовение оказанной услуги - стоимость
            var res24 = invoces.Join(services, i => i.ServiceId, s => s.Id, (i, s) => new
                {
                    name = s.Name,
                    sum = i.Sum
                });

            //+ дополнительные инструменты (такие использовала только 
            //в семестровках по численным методам)
            //Range генерирует последовательность целых чисел
            var res25 = Enumerable.Range(1, 100);  //числа от1  до 100
            //Repeat генерирует последовательность, повторяя указанный 
            //элемент заданное количество раз
            var res26 = Enumerable.Repeat(2, 10);
            //Empty генерирует пустую последовательность заданного типа
            var res27 = Enumerable.Empty<string>();


            //Это были отложенные операции

            //--------------------------------------------

            // а теперь не отложенные операции

            //ToArray создает массив типа T из входной последовательности типа T
            var res28 = students.ToArray();
            //ToList создает List типа T из входной последовательности
            var res29 = (new[] { 1, 2, 3, 4, 5 }).ToList();

            //ToDictionary создает словарь по ключу
            //Используется для, например, оптимизации поиска по идентификатору
            var res30 = services.ToDictionary(x => x.Id);
            //Теперь, чтобы найти услуг по идентификатору, не нужно использовать Where
            //На больших коллекциях Where работает дольше (O(n)), 
            //services.Where(x => x.Id == 2);
            //чем Get из словаря (O(1))
            var serv1 = res30[1];
            //Если сомневаемся, существует ли такой элемент, то TryGetValue
            if (res30.TryGetValue(1, out var serv1try)) //bool - есть такое или нет
            {
                //если есть, то тут используем переменную serv1try
            };

            //Если может быть несколько элементов с одинаковым ключом, то ToLookup
            var servicesWithDuplications = new List<Service1>
            {
                new Service1 { Id = 1, Name = "Замена масла" },
                new Service1 { Id = 1, Name = "Замена масла копия" },
                new Service1 { Id = 2, Name = "Замена колес" },
                new Service1 { Id = 3, Name = "Замена фар" }
            };
            var res31 = servicesWithDuplications.ToLookup(x => x.Id)[1];

            //First, FirstOrDefault, Last, LastOrDefault
            //First возвращает первый элемент последовательности 
            //или первый элемент последовательности, соответствующий предикату
            var res32 = students
                .Where(x => x.Last == "Garcia")
                .OrderBy(x => x.First)
                .First();
            //если элемента нет, то First выкидывает InvalidOperationException
            //В таком случае лучше использовать FirstOrDefault
            //FirstOrDefault возвращает первый элемент, найденный во входной 
            //последовательности. Если последовательность пуста, возвращается default(Т)
            var res33 = students
                .OrderBy(x => x.First)
                .FirstOrDefault(x => x.Last == "Garifullina");
            //Похожим образом работает пара Last, LastOrDefault,
            //Только возвращают последний элемент последовательности
            var res34 = students
                .OrderBy(x => x.First)
                .Last(x => x.Last == "Garcia");
            var res35 = students
               .Where(x => x.Last == "Garifullina")
               .OrderBy(x => x.First)
               .LastOrDefault();

            //Single и SingleOrDefault
            //Single возвращает единственный элемент последовательности
            // если Single не находит элемента для возврата, 
            //генерируется исключение InvalidOperationException
            var res36 = students
               .Where(x => x.Last == "Omelchenko")
               .Single();
            //SingleOrDefault возвращает единственный элемент, найденный 
            //во входной последовательности. Если последовательность пуста, 
            //возвращается default (Т)
            //Если найдено более одного элемента, генерируется InvalidOperationException
            var res37 = students
               .Where(x => x.Last == "Garifullina")
               .SingleOrDefault();
            //ElementAt возвращает элемент по указанному индексу
            //если индекс меньше нуля либо больше или равен количеству элементов
            //то ArgumentOutOfRangeException 
            var res38 = students.ElementAt(1);
            //ElementAtOrDefault возвращает из исходной последовательности элемент, 
            //имеющий указанный индекс местоположения
            //Если заданный индекс меньше нуля либо больше или равен количеству 
            //элементов в последовательности возвращается default(Т)
            var res39 = students.ElementAtOrDefault(100);

            //Any - возвращает true или false в зависимости от того, ест ли элементы 
            //в последовательности или нет (возможно с использованием фильтра)
            var res40 = students.Where(x => x.Last == "Garifullina").Any();
            //идентичный запрос в более короткой форме
            var res41 = students.Any(x => x.Last == "Garifullina");

            //All возвращает true, если каждый элемент входной последовательности 
            //отвечает условию
            var res42 = students
                .Where(x => x.Last == "Garcia")
                .All(x => x.Last == "Garcia");
            var res43 = students
                .All(x => x.Last.Length > 5);

            //Contains возвращает true, если существует элемент, удовлетворяющий условию
            var res44 = students.Select(x => x.Last).Contains("Garcia");
            var res45 = students.Select(x => x.Last).Contains("Garifullina");

            //Count возвращает количество элементов последовательности
            var res46 = students.Count();
            var res47 = students
                .Where(x => x.Last == "Garcia")
                .Count();

            //LongCount возвращает количество элементов последовательности как значение типа long
            var res48 = students
                .Where(x => x.Last == "Garcia")
                .LongCount();

            //Sum возвращает сумму числовых значений, содержащихся в элементах последовательности
            var res49 = students
                .Select(x => x.Scores[0])
                .Sum();
            //Или более короткий вариант
            var res50 = students
                .Sum(x => x.Scores[0]);

            //Min возвращает минимальное значение входной последовательности
            var res51 = students
                .Min(x => x.Scores[0]);
            //Max возвращает максимальное значение из входной последовательности
            var res52 = students
                .Max(x => x.Scores[0]);

            // Average возвращает среднее арифметическое числовых значений элементов
            var res53 = students
                .Average(x => x.Scores[0]);

            //Aggregate выполняет указанную пользователем функцию на каждом элементе 
            //входной последовательности, передавая значение, 
            //возвращенное этой функцией для предыдущего элемента, 
            //и возвращая ее значение для последнего элемента
            //Вычисление 5!
            var res54 = Enumerable
                .Range(1, 5)
                .Aggregate((av, e) => av * e);

        }
    }

    /// <summary>
    /// Услуга
    /// </summary>
    class Service1
    {
        /// <summary>
        /// Идентификатор услуги
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Наименование услуги
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// Тип услуги (просто чтобы полей побольше было для примера)
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// Пользователь, добавивший в справочник 
        /// (просто чтобы полей побольше было для примера)
        /// </summary>
        public int User { get; set; }
    }

    /// <summary>
    /// Счет за оказанные услуги
    /// </summary>
    class Invoice 
    {
        /// <summary>
        /// Идентификатор услуги
        /// </summary>
        public int ServiceId { get; set; }
        /// <summary>
        /// Сумма
        /// </summary>
        public double Sum { get; set; }
    }
}


