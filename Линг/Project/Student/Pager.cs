﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Student
{
    public class Pager
    {
        public void Register(MailManager mm)
        {
            mm.NewMail += PagerMsg;
        }

        public void Unregister(MailManager mm)
        {
            // Отменить регистрацию на уведомление о событии NewMail объекта MailManager.
            mm.NewMail -= PagerMsg;
        }

        private void PagerMsg(Object sender, NewMailEventArgs e)
        {
            Console.WriteLine("Pagering mail message:");
            Console.WriteLine(" From={0}, To={1}, Subject={2}",
            e.From, e.To, e.Subject);
        }
    }
}
