﻿using System;
using System.Collections.Generic;
using System.Text;
using Student;

namespace Runner
{
    /// <summary>
    /// Запуск примеров событий
    /// </summary>
    public class EventRunner
    {
        public void Run() {

            var mm = new MailManager();
            var fax = new Fax();
            var pager = new Pager();

            fax.Register(mm);

            mm.SimulateNewMail("me", "you", "Hi, my dear friend");

            pager.Register(mm);

            mm.SimulateNewMail("Михаилу", "Ренаты", "У нас пара, приходи");


            var worker = new Worker();
            var manager = new Manager();
            var fd = new FinDepartment();

            manager.Register(worker);
            fd.Register(worker);

            var param = new GoVacationParams() 
            {
                DateFrom = new DateTime(2020, 3, 23),
                DateTo = new DateTime(2020, 4, 5)
            };

            worker.GoVacation(param);

            manager.UnRegister(worker);

            param = new GoVacationParams()
            {
                DateFrom = new DateTime(2020, 4, 13),
                DateTo = new DateTime(2020, 4, 26),
                IsAdmVacation = true
            };

            worker.GoVacation(param);
        }

    }
}
