﻿using System;
using Student;

namespace Runner
{
    class Program
    {
        static void Main(string[] args)
        {
            var runner = new LinqTask();
            runner.Run();
        }

    }
}
