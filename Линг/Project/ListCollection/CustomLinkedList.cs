﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ListCollection
{
    /// <summary>
    /// Двунаправленный связный список
    /// </summary>
    public class CustomLinkedList<T> : ICustomCollection<T>
        where T : IComparable<T>
    {
        private LinkedNode<T> head;

        ///<inheritdoc/>
        public void Add(T item)
        {
            if (IsEmpty())
            {
                head = new LinkedNode<T>() { Data = item };
            }
            else
            {
                var node = head;
                while(node.NextNode!=null)
                {
                    node = node.NextNode;
                }
                var newNode = new LinkedNode<T>() { Data = item, PrevNode = node };
                node.NextNode = newNode;
            }
        }
        ///<inheritdoc/>
        public void AddRange(T[] items)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public void Clear()
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public bool Contains(T data)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public int IndexOf(T item)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public void Insert(int index, T item)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public bool IsEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/>
        public void Remove(T item)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public void RemoveAll(T item)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            //проверка на выход за границы коллекции
            if (index < 0 || index > (Size() - 1))
                throw new IndexOutOfRangeException();


            //Удаление первого элемента
            if (index == 0)
            {
                head = head.NextNode;
                head.PrevNode = null;
            }
            else
            {
                var node = head;
                for (var i = 0; i < index; i++) {
                    node = node.NextNode;
                }
                node.NextNode = node.NextNode.NextNode;
                node.NextNode.PrevNode = node;
            }
        }
        ///<inheritdoc/>
        public void Reverse()
        {
            //по желанию
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public int Size()
        {
            //todo homework
            throw new NotImplementedException();
        }
    }
}
