﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ListCollection
{
    /// <summary>
    /// Коллекция на основе массива
    /// </summary>
    public class ArrayList<T> : IEnumerable<T>, ICustomCollection<T>
        where T : IComparable<T>
    {
        T[] mas;
        /// <summary>
        /// Сколько ячеек с информацией в массиве
        /// </summary>
        int count = 0; 

        public void Add(T item)
        {
            if (IsEmpty())
            {
                mas = new T[1] { item };
                count = 1;
            }
            else 
            {
                if (count == mas.Length)
                {
                    var newMas = new T[mas.Length * 2];
                    for (int i = 0; i < mas.Length; i++)
                    {
                        newMas[i] = mas[i];
                    }
                    mas = newMas;
                }

                mas[count] = item;
                count++;
            }
        }

        public void AddRange(T[] items)
        {
            //todo homework
            throw new NotImplementedException();
        }

        public void Clear()
        {
            //todo homework
            throw new NotImplementedException();
        }

        public bool Contains(T data)
        {
            //todo homework - можно использовать готовое
            throw new NotImplementedException();
        }

        public IEnumerator<T> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        public int IndexOf(T item)
        {
            //todo homework - можно использовать готовое
            throw new NotImplementedException();
        }

        public void Insert(int index, T item)
        {
            //todo homework
            throw new NotImplementedException();
        }

        public bool IsEmpty()
        {
            return count == 0;
        }

        public void Remove(T item)
        {
            //todo homework
            throw new NotImplementedException();
        }

        public void RemoveAll(T item)
        {
            //todo homework
            throw new NotImplementedException();
        }

        public void RemoveAt(int index)
        {
            //todo homework
            throw new NotImplementedException();
        }

        public void Reverse()
        {
            //todo homework
            throw new NotImplementedException();
        }

        public int Size()
        {
            return count;
        }

        public override string ToString() 
        {
            string result = string.Empty;
            for (int i = 0; i < count; i++) {
                result += " " + mas[i].ToString();
            }
            return result;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            //todo homework
            throw new NotImplementedException();
        }
    }
}
