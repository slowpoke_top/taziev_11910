﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ListCollection
{
    /// <summary>
    /// Линейный односвязный список
    /// </summary>
    public class CustomList<T> : IEnumerable<T>, ICustomCollection<T> where T: IComparable<T>
    {
        private Node<T> head;
        ///<inheritdoc/>
        public void Add(T item)
        {
            if (IsEmpty()) head = new Node<T>() {Data = item};
            else
            {
                Node<T> node = head;
                while (node.NextNode != null) node = node.NextNode;
                node.NextNode = new Node<T>() { Data = item };
            }
        }
        ///<inheritdoc/>
        public void AddRange(T[] items)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public void Clear()
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public bool Contains(T data)
        {
            Node<T> curNode = head;

            while (curNode != null)
                if (curNode.Data.CompareTo(data) == 0)
                    return true;
                else
                    curNode = curNode.NextNode;

            return false;
        }
        ///<inheritdoc/>
        public int IndexOf(T item)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public void Insert(int index, T item)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public bool IsEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/>
        public void Remove(T item)
        {
            //проверка списка на пустоту
            if (IsEmpty())
            {
                return;
            }

            //Значение - головной элемент
            if (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                return;
            }

            //идем до конца списка
            Node<T> node = head;
            while(node.NextNode != null 
                && node.NextNode.Data.CompareTo(item) != 0) 
            {
                node = node.NextNode;
            }

            if (node.NextNode != null) 
            {
                node.NextNode = node.NextNode.NextNode;
            }
        }
        ///<inheritdoc/>
        public void RemoveAll(T item)
        {    
            //проверка списка на пустоту
            if (IsEmpty())
            {
                return;
            }

            //Удаляю с головы занчения item
            while (head.Data.CompareTo(item) == 0)
            {
                head = head.NextNode;
                if(head == null) return;
            }

            RemoveFromElement(head, item);
        }

        private void RemoveFromElement(Node<T> node, T item) 
        {
            if (node.NextNode == null)
                return;
            if (node.NextNode.Data.CompareTo(item) != 0)
            {
                node = node.NextNode;
            }
            else 
            {
                node.NextNode = node.NextNode.NextNode;
            }
            RemoveFromElement(node, item);
        }

        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        public void Reverse()
        {
            //todo homework
            throw new NotImplementedException();
        }
        ///<inheritdoc/>
        ///<inheritdoc/>
        public void Display()
        {
            if (!IsEmpty())
            {
                Node<T> node = head;
                while (node != null)
                {
                    Console.Write(node.Data + " ");
                    node = node.NextNode;
                }

                Console.WriteLine();
            }
            else Console.WriteLine("list empty");
        }
        public int Size()
        {
            Node<T> node = head;
            int size = 0;
            while (node.NextNode != null)
            {
                size++;
                node = node.NextNode;
            }
            return size;
        }

        public IEnumerator<T> GetEnumerator()
        {
            return new NodeEnumerator<T>(head);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }

    public class NodeEnumerator<T> : IEnumerator<T>
    {
        Node<T> node;
        Node<T> head;
        public NodeEnumerator(Node<T> h)
        {
            head = h;
            node = new Node<T>() { Data = default(T), NextNode = head };
        }

        T IEnumerator<T>.Current => node.Data;

        object IEnumerator.Current => node.Data;

        public bool MoveNext()
        {
            if (node.NextNode != null)
            {
                node = node.NextNode;
                return true;
            }
            else
                return false;
        }
        public void Reset()
        {
            node = new Node<T>() { Data = default(T), NextNode = head };
        }

        public void Dispose()
        {      
        }
    }
}
