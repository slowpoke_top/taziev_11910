﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ListCollection;
using Student;

namespace ListTest
{
    [TestClass]
    public class ArrayListTest
    {
        [TestMethod]
        public void IsEmptyTest() 
        {
            var arrayList = new ArrayList<Student.Student>();
            Assert.IsTrue(arrayList.IsEmpty());
            Assert.AreEqual(arrayList.ToString(), string.Empty);
        }

        [TestMethod]
        public void AddTest()
        {
            var arrayList = new ArrayList<string>();
            Assert.IsTrue(arrayList.IsEmpty());
            arrayList.Add("5");
            Assert.IsFalse(arrayList.IsEmpty());
            arrayList.Add("75");
            Assert.AreEqual(arrayList.ToString(), " 5 75");
        }
    }
}
