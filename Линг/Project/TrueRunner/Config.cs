﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Student;

namespace TrueRunner
{    
    public class Config
    {
        public TypeEnum InputType { get; set; }
        public ConverterEnum Converter { get; set; }
        public OutPutEnum OutPut { get; set; }
    }

    public enum TypeEnum
    {
        intType,
        stringType
    }

    public enum ConverterEnum 
    { 
        converter1,
        converter2
    }

    public enum OutPutEnum 
    { 
        output1,
        output2
    }
}
