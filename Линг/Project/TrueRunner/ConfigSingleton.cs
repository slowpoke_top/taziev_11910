﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrueRunner
{
    public static class ConfigSingleton
    {
        private static Config conf;
        public static Config getConfig()
        {
            if (conf == null)
            {
                conf = new Config() 
                {
                    InputType = TypeEnum.intType,
                    Converter = ConverterEnum.converter1,
                    OutPut = OutPutEnum.output1
                };
            }
            return conf;
        }
    }
}
