﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

/// <summary>
    /// просто на 3 балла, сложный на 5 баллов
    /// </summary>
    public class Lin
    {
        public class Tovar
        {
            public int count;
            public int id;
            public int price;
            public string name;

            public Tovar(int c,int i,int p, string a )
            {
                count = c;
                id = i;
                price = p;
                name = a;
            }
        }

        public class Shet
        {
            private int Summ = 0;
            private List<Tovar> shet;
            public void AddTovar(int id,int price,int count, string name)
            {
                shet.Add(new Tovar(count,id,price, name));
            }

            public void Sum()
            {
                foreach (var VARIABLE in shet)
                {
                    Summ += (VARIABLE.price * VARIABLE.count);
                }
            }

            public void Vivod()
            {
                Sum();
                foreach (var a in shet)
                {
                    Console.WriteLine("Наименование товара - " + a.name);
                    Console.WriteLine("Сумма всех товаров равна - " + Summ);
                    Console.WriteLine();
                }
            }
        }
        public class Product
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public class Price 
        {
            public int Id { get; set; }
            public int ProductId { get; set; }
            public decimal Sum { get; set; }
            public bool IsActual { get; set; }
        }

        public static void Run() 
        {
            var products = new List<Product>
            {
                new Product { Id = 1, Name = "Аквариум 10 литров" },
                new Product { Id = 2, Name = "Аквариум 20 литров" },
                new Product { Id = 3, Name = "Аквариум 50 литров" },
                new Product { Id = 4, Name = "Аквариум 100 литров" },
                new Product { Id = 5, Name = "Аквариум 200 литров" },
                new Product { Id = 6, Name = "Фильтр" },
                new Product { Id = 7, Name = "Термометр" }
            };

            var prices = new List<Price>
            {
                new Price { Id = 1, ProductId = 1, Sum = 100, IsActual = false },
                new Price { Id = 2, ProductId = 1, Sum = 123, IsActual = true },
                new Price { Id = 3, ProductId = 2, Sum = 234, IsActual = true },
                new Price { Id = 4, ProductId = 3, Sum = 532, IsActual = true },
                new Price { Id = 5, ProductId = 4, Sum = 234, IsActual = true },
                new Price { Id = 6, ProductId = 5, Sum = 534, IsActual = true },
                new Price { Id = 7, ProductId = 5, Sum = 124, IsActual = false },
                new Price { Id = 8, ProductId = 6, Sum = 153, IsActual = true },
                new Price { Id = 9, ProductId = 7, Sum = 157, IsActual = true }
            };
            //1 задание
            var chet = new Shet();
            int count = 5;
            string aea = "Аквариум 200 литров";
            //chet.AddTovar(Int32.Parse(products.Where(x => x.Name == aea).Select(x => x.Id).ToString()), 
            //    Int32.Parse(prices.Where(x => x.ProductId == (Int32.Parse(products.Where(y => y.Name == aea).Select(y => y.Id).ToString()))).ToString()),
            //    count, aea);
            //2 задание
            //chet.Vivod();
            //3 задание
            double[] str = prices.GroupBy(x => x.ProductId).Select(x => x).ToList().Select(x => x.Average()).ToArray();
            foreach (var a in str)
            {
                Console.WriteLine(a);
            }
            Console.WriteLine();


            /*Задания              * 
             * 1) создать список счетов (один счет содержит несколько пар цена-количество)
             * например, один счет - это аквариум на 200 литров, два фильтра и термометр
             * 2) вывести счет для покупателя с колонками "Наименование услуги, сумма, итого"
             *  - 1 балл
             * 3) Вывести среднюю цену для каждого продукта с учетом неактуальных значений
             * - 1 балл
             * 4) создать список акций (код продукта, скидка):
             * аквариум на 200 литров + 2 фильтра - скидка 15%
             * аквариум 100 литров + 2 фильтра - 10% скидка
             * любой другой аквариум + фильтр - 5% скидка
             * 5) создать список перенчень всех названий товаров в группе акции + 
             * цена до скидки + цена с учетом скидки  - 1 балл
             * 
             * Для тех, кто выбрал вариант посложнее: написать функцию подсчета 
             * суммы покупки (выявлять, есть ли в наборе продуктов акционные комплекты,
             * при их наличии делать скидку) - это + 2 балла
             */
        }
    }