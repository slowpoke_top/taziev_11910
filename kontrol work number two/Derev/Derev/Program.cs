﻿using System;

namespace Derev
{
    internal class Program
    {
        public class Tree
        {
            public class TreeNode
            {
                public int Value; 
                public int Count = 0; 
                public TreeNode Left; 
                public TreeNode Right; 
            }
            public TreeNode Node;
        }

        private void AddRecursion(ref Tree.TreeNode node, int val)
        {
            throw new System.NotImplementedException();
        }

        private void PrintRecursion(Tree.TreeNode node, int spaces, ref string s)
        {
            throw new System.NotImplementedException();
        }

        private void Across(Tree.TreeNode node, ref string s, bool detailed)
        {
            throw new System.NotImplementedException();
        }

        private void RCL(Tree.TreeNode node, ref string s, bool detailed)
        {
            if (node != null)
            {
                if (detailed) s += "    обходим правое поддерево" + Environment.NewLine;
                RCL(node.Right, ref s, detailed); // обойти правое поддерево
                if (detailed)
                    s += "    получили значение " + node.Value.ToString() + Environment.NewLine;
                else
                    s += node.Value.ToString() + " "; // запомнить текущее значение
                if (detailed) s += "    обходим левое поддерево" + Environment.NewLine;
                RCL(node.Left, ref s, detailed); // обойти левое поддерево
            }
            else if (detailed) s += "    значение отсутствует - null" + Environment.NewLine;
        }

        public static void Main(string[] args)
        {
            
        }
    }
}