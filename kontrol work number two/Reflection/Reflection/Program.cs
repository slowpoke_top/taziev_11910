﻿using System;
using System.Reflection;

namespace Reflection
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            var assembly = 
                Assembly.LoadFrom(@"C:\Users\Frameworker\taziev_11910\kontrol work number two\Rogdenie\Rogdenie\bin\Debug\Rogdenie.exe");
            Type type = assembly.GetType("Rogdenie.Woman");
            object classForReflection =  Activator.CreateInstance(type, new object[] { "ZArina" });
            var flags = BindingFlags.NonPublic | BindingFlags.Static;
            FieldInfo publicField = type.GetField("FieldForReflection");    
            object publicFieldValue = publicField.GetValue(classForReflection);
            Console.WriteLine(publicFieldValue.ToString());
            flags = BindingFlags.NonPublic | BindingFlags.Instance;
            FieldInfo privateField = type.GetField("_name", flags);
            object privateFieldValue = privateField.GetValue(classForReflection);
            Console.WriteLine(privateFieldValue.ToString());
        }
    }
}