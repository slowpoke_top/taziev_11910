﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Rogdenie
{
    public class VacationParams
    {
        public DateTime _date;
        private string _name;
        
        public DateTime Date{ get { return _date; } }
        public string Name{ get { return _name; } }


        public VacationParams(DateTime date, string name)
        {
            _date = date;
            _name = name;
        }
    }
}