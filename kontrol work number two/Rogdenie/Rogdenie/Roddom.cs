﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rogdenie
{
    public class Roddom
    {
        public void Register(Woman woman) 
        {
            woman.GoOnVacationEvent += FindReplacement; 
        }
        public void Unregister(Woman woman) 
        {
            woman.GoOnVacationEvent -= FindReplacement;
        }
        private void FindReplacement(Woman woman, VacationParams param) 
        {
            Console.WriteLine("Я роддом заполняю доки " +
                              woman.Name + " на дату рождения ребенка " +
                               param.Date.ToShortDateString());
        }
    }
}