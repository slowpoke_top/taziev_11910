﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rogdenie
{
    class Program
    {
        static void Main(string[] args)
        {
            var zags = new Zags();
            var woman = new Woman("Zarina");
            var roddom = new Roddom();
            var sad = new VacationParams(new DateTime(2020, 1, 30),"DiteZarini");
             
            zags.Register(woman);
            roddom.Register(woman);
            woman.GoOnDecret(sad);
        }
    }
}