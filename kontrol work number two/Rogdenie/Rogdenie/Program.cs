﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rogdenie
{
    public class Woman
    {
        /// <summary>
        /// Событие уход в отпуск
        /// </summary>
        public Action<Woman, VacationParams> GoOnVacationEvent;

        public string FieldForReflection = "FIELD";
        private string _name;
        public string Name { get { return _name; } }

        public Woman(string womanName) 
        {
            _name = womanName;
        }
        public void GoOnDecret(VacationParams param) 
        {
            if (GoOnVacationEvent != null)
                GoOnVacationEvent(this, param);
        }
    }
}