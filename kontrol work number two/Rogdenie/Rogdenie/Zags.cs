﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rogdenie
{
    public class Zags
    {
        public void Register(Woman mm) 
        {
            mm.GoOnVacationEvent += FaxMessage;
        }
        public void Unregister(Woman mm)
        {
            mm.GoOnVacationEvent -= FaxMessage;
        }
        

        private void FaxMessage(object sender, VacationParams args) 
        {
            Console.WriteLine("Я заполняю свидетельство о рождение " + args.Name);
        }
    }
}