﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /// <summary>
    /// Двунаправленный список
    /// </summary>
    public class CustomLinkedList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        private LinkedNode<T> head;

        ///<inheritdoc/>
        public void Add(T elem)
        {
            if (head == null)
                head = new LinkedNode<T>() { Data = elem };
            else
            {
                var node = head;
                while (node.NextNode != null) {
                    node = node.NextNode;
                }
                var newNode = new LinkedNode<T>() { Data = elem };
                node.NextNode = newNode;
                newNode.PrevNode = node;
            }
        }

        ///<inheritdoc/>
        public void AddRange(T[] elems)
        {
            //todo домой
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Clear()
        {
            //todo домой
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public bool Contains(T elem)
        {
            //todo домой
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public int IndexOf(T elem)
        {
            //todo домой
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Insert(int index, T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public bool isEmpty()
        {
            //todo домой
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Remove(T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void RemoveAll(T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Reverse()
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public int Size()
        {
            //todo домой
            throw new NotImplementedException();
        }

        /// <summary>
        /// Вывести список строкой
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (head == null) return null;
            var node = head;
            var sb = new StringBuilder();
            while (node != null)
            {
                sb.Append(" " + node.Data.ToString());
                node = node.NextNode;
            }
            return sb.ToString();
        }
    }
}
