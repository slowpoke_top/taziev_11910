﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /// <summary>
    /// Узел бинарного (двоичного дерева поиска)
    /// </summary>
    public class BTreeNode<T>
    {
        public BTreeNode(int key, T data, BTreeNode<T> parent = null)
        {
            Data = data;
            Key = key;
            Parent = parent;
        }

        /// <summary>
        /// Ключ
        /// </summary>
        public int Key { get; set; }

        /// <summary>
        /// данные
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// левая ветка дерева
        /// </summary>
        public BTreeNode<T> Left { get; set; }

        /// <summary>
        /// правая ветка дерева
        /// </summary>
        public BTreeNode<T> Right { get; set; }

        /// <summary>
        /// Родительский элемент
        /// </summary>
        public BTreeNode<T> Parent { get; set; }
    }
}
