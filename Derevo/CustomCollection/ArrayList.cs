﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /// <summary>
    /// Коллекция на основе массива
    /// </summary>
    public class ArrayList<T> : ICustomCollection<T>, IEnumerable
        where T : IComparable<T>
    {
        /// <summary>
        /// Массив с данными
        /// </summary>
        private T[] array;
        /// <summary>
        /// Количество заполненных ячеек массива 
        /// </summary>
        private int size;

        ///<inheritdoc/>
        public void Add(T elem)
        {
            //добавление в пустой список
            if (isEmpty()) {
                array = new T[1] { elem };
                size = 1;
                return;
            }
            //Список не пустой, место есть
            if (array.Length > size)
            {
                array[size] = elem;
                size += 1; //size = size + 1;
            }
            else 
            {
                var newArray = new T[array.Length * 2];
                for (int i = 0; i < size; i++)
                    newArray[i] = array[i];
                newArray[size] = elem;
                size += 1;
                array = newArray;
            }
        }

        ///<inheritdoc/>
        public void AddRange(T[] elems)
        {
            //todo homework 26.02
            if (isEmpty())
            {
                array = new T[elems.Length];
                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = elems[i];
                }
                return;

            }
            // список не пустой => добавляем в конец
            if (array.Length > size)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    array[size] = elems[i];
                    size++;
                }
            }
            else
            {
                var newArray = new T[elems.Length * 2];
                for (int i = 0; i < size; i++)
                    newArray[i] = array[i];
                int k = size;
                int j = 0;
                while (j != newArray.Length)
                {
                    newArray[k] = elems[j];
                    k++;
                    j++;
                }
            }
        }

        public void Clear()
        {
            size = 0;
            array = null;
        }

        ///<inheritdoc/>
        public bool Contains(T elem)
        {
            //todo homework 26.02
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public int IndexOf(T elem)
        {
            //todo homework 26.02
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Insert(int index, T elem)
        {
            //todo homework 26.02
            throw new NotImplementedException();
        }

        public bool isEmpty()
        {
            return size == 0;
        }

        ///<inheritdoc/>
        public void Remove(T elem)
        {
            if (isEmpty()) return;
            var position = 0;
            while (position < size && array[position].CompareTo(elem) != 0) 
            {
                position++;
            }
            if (position < size - 1) 
            {
                for (int i = position; i < size; i++) 
                {
                    array[i] = array[i + 1];
                }
            }
            if (position < size)
            {
                array[size - 1] = default(T);
                size--;
            }
        }

        ///<inheritdoc/>
        public void RemoveAll(T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Reverse()
        {
            Array.Resize<T>(ref array, size);
            Array.Reverse(array);
        }

        ///<inheritdoc/>
        public int Size()
        {
            return size;
        }
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            Array.Resize<T>(ref array, size);
            return array.GetEnumerator();
        }
    }
}
