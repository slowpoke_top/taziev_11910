﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /*
      Также как и для последовательностей, доступ к компонентам (вершинам) дерева может
      быть определен как прямой или последовательный. Будем различать понятия: позиция
      вершины дерева - однозначно идентифицирующая вершину и её положение в дереве, и
      элемент – информация, ассоциированная с вершиной дерева (в частности, просто
      хранимая вместе с ней).
      
      В основу представления бинарного дерева может быть положен принцип
      нумерации вершин. Каждая вершина дерева получает порядковый номер p(v):
      • если v является корнем, то p(v) = 1;
      • если v - левый сын вершины u, то p(v) = 2*p(u);
      • если v - правый сын вершины u, то p(v) = 2*p(u)+ l.   
         */


    /// <summary>
    /// Бинарное дерево поиска
    /// </summary>
    public class BinarySearchTree<T>
    {
        //!!!! Сигнатура методов может быть изменена, обсудим
        private BTreeNode<T> root;

        /// <summary>
        /// возвращает значение корня дерева
        /// изначально надо было вернуть позицию
        /// </summary>
        public T Root() 
        {
            return root != null ? root.Data : default(T);
        }

        /// <summary>
        /// возвращает значение «родителя» для вершины в позиции p
        /// изначально надо было вернуть позицию
        /// </summary>
        public T Parent(int p)
        {
            if (p < 2) throw new ArgumentException("Элемент не имеет родителя");
            List<int> way = new List<int>(); //Путь от позиции p до корня
            while (p > 1) 
            {
                way.Insert(0, p);
                p = p / 2;
            }
            var rootCopy = root;
            for (int i = 0; i < way.Count; i++) 
            {
                if (way[i] % 2 == 0) //идем в левое поддерево
                {
                    if (rootCopy.Left == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null) throw
                             new ArgumentOutOfRangeException($"Элемента на позиции {p} нет");
                    rootCopy = rootCopy.Right;
                }
            }
            return rootCopy != null ? rootCopy.Parent.Data : default(T);
        }

        /// <summary>
        /// возвращает значение «самого левого сына» для вершины в позиции p.
        /// изначально надо было вернуть позицию
        /// </summary>
        public T LeftMostChild(int p)
        {
            //todo homework
            throw new NotImplementedException();
        }

        /// <summary>
        /// возвращает значение «правого брата» для вершины в позиции p.
        /// изначально надо было вернуть позицию
        /// </summary>
        public T RightSibling(int p)
        {
            //по желанию
            throw new NotImplementedException();
        }

        /// <summary>
        /// возвращает элемент дерева (хранимую информацию) для вершины в позиции p.
        /// </summary>
        public T Element(int p)
        {
            //todo homework
            throw new NotImplementedException();
        }

        /// <summary>
        /// проверяет, является ли p позицией внутренней вершины (не листа)
        /// </summary>
        public bool IsInternal(int p)
        {
            //todo homework
            throw new NotImplementedException();
        }

        /// <summary>
        /// проверяет, является ли p позицией листа дерева.
        /// </summary>
        public bool IsExternal(int p)
        {
            //todo homework
            throw new NotImplementedException();
        }

        /// <summary>
        /// проверяет, является ли p позицией корня.
        /// </summary>
        public bool IsRoot(int p)
        {
            if (root == null) throw new ArgumentException("Дерево пустое");
            return p == 1;
        }

        /// <summary>
        /// проверяет, является ли key ключом корневого узла.
        /// </summary>
        public bool IsRootByKey(int key)
        {
            if (root == null) throw new ArgumentException("Дерево пустое");
            return root.Key == key;
        }

        /// <summary>
        /// Поиск элемента в дереве
        /// </summary>
        public bool Find(int key)
        {
            //todo homework
            throw new NotImplementedException();
        }

        /// <summary>
        /// добавление в дерево значения 
        /// </summary>
        public void Insert(int key, T data)
        {
            if (root == null)
            { 
                root = new BTreeNode<T>(key, data);
                return;
            }
            var rootCopy = root;
            bool searchPosition = true;
            while (searchPosition) 
            {
                if (rootCopy.Key == key) 
                {
                    rootCopy.Data = data;
                    return;
                }
                if (rootCopy.Key > key)
                {
                    if (rootCopy.Left == null)
                    { 
                        rootCopy.Left = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Left;
                }
                else
                {
                    if (rootCopy.Right == null)
                    { 
                        rootCopy.Right = new BTreeNode<T>(key, data, rootCopy);
                        searchPosition = false;
                    }
                    else
                        rootCopy = rootCopy.Right;
                }
            }
        }

        /// <summary>
        /// удаление узла, в котором хранится значение
        /// </summary>
        public void Remove(int key)
        {
            //доделать по желанию
            if(root == null) throw new ArgumentException("Дерево пустое");
            var rootCopy = root;
            var search = true;
            while (search) 
            {
                if (rootCopy.Key > key)
                {
                    if (rootCopy.Left == null)
                        throw new ArgumentException($"Элемент с ключом {key} отсутствует");
                    rootCopy = rootCopy.Left;
                }
                else if (rootCopy.Key < key)
                {
                    if (rootCopy.Right == null)
                        throw new ArgumentException($"Элемент с ключом {key} отсутствует");
                    rootCopy = rootCopy.Right;
                }
                else //ключи совпадают 
                {
                    search = false;
                }
            }
        }

        //https://learnc.info/adt/binary_tree_traversal.html вывод деревьев

        /// <summary>
        /// Вывод в глубину прямой
        /// Прямой (pre-order)        
        /// Посетить корень    
        /// Обойти левое поддерево    
        /// Обойти правое поддерево
        /// </summary>
        public void PreOrderPrint()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Вывод в глубину Симметричный или поперечный (in-order)
        /// Обойти левое поддерево
        /// Посетить корень
        /// Обойти правое поддерево
        /// </summary>
        public void InOrderPrint()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Вывод в глубину В обратном порядке (post-order)
        /// Обойти левое поддерево
        /// Обойти правое поддерево
        /// Посетить корень
        /// </summary>
        public void PostOrderPrint()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Вывод в ширину
        /// </summary>
        public void PrintDepth()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Сбалансировать дерево *
        /// </summary>
        public void Balance() 
        { 
        }
    }
}
