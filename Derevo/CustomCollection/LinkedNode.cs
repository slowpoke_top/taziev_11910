﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /// <summary>
    /// Элемент двунаправленного связного спика
    /// </summary>
    public class LinkedNode<T>
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// Ссылка на предыдущий элемент
        /// </summary>
        public LinkedNode<T> PrevNode { get; set; }
        /// <summary>
        /// Ссылка на след элемент
        /// </summary>
        public LinkedNode<T> NextNode { get; set; }
    }
}
