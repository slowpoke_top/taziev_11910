﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomCollection;

namespace ConsoleApp1
{
    /// <summary>
    /// Для запуска примеров
    /// </summary>
    public class Runner
    {
        public void Run() 
        {
            var list = new CustomList<int>();
            list.Add(5);
            list.Add(7);
            list.Add(9);
            list.Add(5);
            list.Add(13);
            list.Add(66);
            list.Add(5);

            foreach (var item in list) {
                Console.WriteLine(item);
            }

            //list.RemoveAll(5);
            //list.Reverse();
            Console.WriteLine(list.ToString());
            Console.ReadKey();
        }
    }
}
