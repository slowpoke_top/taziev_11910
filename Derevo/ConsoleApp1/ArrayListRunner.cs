﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomCollection;

namespace ConsoleApp1
{
    public class ArrayListRunner
    {
        public void Run() {
            var studends = new ArrayList<Student>();
            studends.Add(new Student
            {
                City = "Казань",
                Fio = "Гарифуллина Рената Наилевна",
                Birthday = new DateTime(1800, 5, 5),
                Mark = 4.38953F
            });
            studends.Add(new Student
            {
                City = "Казань",
                Fio = "Абрамский Михаил Михайлович",
                Birthday = new DateTime(1800, 5, 5),
                Mark = 5F
            });
            studends.Add(new Student
            {
                City = "Казань",
                Fio = "Самитов Ренат Касимович",
                Birthday = new DateTime(1800, 5, 5),
                Mark = 5F
            });


            studends.Remove(new Student
            {
                City = "Казань",
                Fio = "Гарифуллина Рената Наилевна",
                Birthday = new DateTime(1800, 5, 5),
                Mark = 4.38953F
            });

            studends.Remove(new Student
            {
                City = "Казань",
                Fio = "Самитов Ренат Касимович",
                Birthday = new DateTime(1800, 5, 5),
                Mark = 5F
            });


            foreach (var s in studends)
            {
                Console.WriteLine(s.ToString());
            }
        }
    }
}
