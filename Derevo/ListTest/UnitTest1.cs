﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomCollection;

namespace ListTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var list = new CustomList<int>();
            list.Add(5);
            list.Add(7);
            list.Add(9);
            list.Add(5);
            list.Add(13);
            list.Add(66);
            list.Add(5);
            //list.AddRange(new []{ 4,2,5,6,15,3,2});

            list.RemoveAll(5);
            list.Reverse();
        }
    }
}
