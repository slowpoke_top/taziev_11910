﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CustomCollection;


namespace ListTest
{
    [TestClass]
    public class TreeTest
    {
        [TestMethod]
        public void TestInsert()
        {
            var tree = new BinarySearchTree<string>();
            tree.Insert(8, "восемь");
            tree.Insert(3, "три");
            tree.Insert(10, "лалала");
            tree.Insert(10, "десять");
            tree.Insert(14, "четырнадцать");
        }
    }
}
