﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collections
{
    /// <summary>
    /// Элемент однонаправленного списка
    /// </summary>
    public class Node<T>
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public T Data;

        /// <summary>
        /// Ссылка на след элемент
        /// </summary>
        public Node<T> NextNode;
    }
}
