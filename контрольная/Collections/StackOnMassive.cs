﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Collections
{
    /// <summary>
    /// Стек на основе массива 1
    /// </summary>
    public class StackOnMassive<T>
    {
        private T[] elements;

        /// <summary>
        /// Добавление
        /// </summary>
        public void Push(T item) 
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаление
        /// </summary>
        public T Pop()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Чтение первого элемента
        /// </summary>
        public T Peek()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        public bool IsEmpty()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Размер коллекции
        /// </summary>
        public int Size()
        {
            throw new NotImplementedException();
        }
    }
}
