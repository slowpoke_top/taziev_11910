﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace Collections
{
    /// <summary>
    /// Очередь на двух листах - посложнее
    /// </summary>
    public class QueueOnTwoList<T>
    {
        private Node<T> rightListHead;
        private Node<T> leftListHead;

        /// <summary>
        /// Добавление
        /// </summary>
        public void Push(T item)
        {
            leftListHead = new Node<T>() {Data = item, NextNode = leftListHead};
        }

        /// <summary>
        /// Удаление
        /// </summary>
        public T Pop()
        {
            if (rightListHead == null)
            {
                if (leftListHead != null)
                {
                    rightListHead = new Node<T>() {Data = leftListHead.Data};
                    var noder = rightListHead;
                    while (leftListHead != null)
                    {
                        var a = new Node<T>() {Data = leftListHead.Data, NextNode = noder};
                        noder = a;
                        leftListHead = leftListHead.NextNode;
                    }

                    rightListHead = noder;
                    var ad = rightListHead.Data;
                    rightListHead = rightListHead.NextNode;
                    return ad;
                } else throw new Exception("This is your problem...");
            }
            else 
            {
                var ad = rightListHead.Data;
                rightListHead = rightListHead.NextNode;
                return ad;
            }
        }

        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        public bool IsEmpty()
        {
            return (rightListHead == null && leftListHead == null);
        }

        /// <summary>
        /// Размер коллекции
        /// </summary>
        public int Size()
        {
            if (leftListHead == null && rightListHead == null) return 0;
            var node = leftListHead;
            var noder = rightListHead;
            var counter = 1;
            if (leftListHead != null && rightListHead != null)
            {
                
                while (noder.NextNode != null)
                {
                    counter++;
                    noder = noder.NextNode;
                }
                while (node.NextNode != null)
                {
                    counter++;
                    node = node.NextNode;
                }
                return counter;
            }
            else if (rightListHead != null)
            {
                while (noder.NextNode != null)
                {
                    counter++;
                    noder = noder.NextNode;
                }
                return counter;
            }
            else
            {
                while (node.NextNode != null)
                {
                    counter++;
                    node = node.NextNode;
                }
                return counter;
            }
        }
    }
}
