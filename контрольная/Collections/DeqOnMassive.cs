﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collections
{
    /// <summary>
    /// Дек на основе массива - 6
    /// </summary>
    public class DeqOnMassive<T>
    {
        private T[] elements;

        /// <summary>
        /// Добавление в конец
        /// </summary>
        public void PushBack(T item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаление с конца
        /// </summary>
        public T PopBack()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Добавление в начало
        /// </summary>
        public void PushFront(T item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаление с начала
        /// </summary>
        public T PopFront()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        public bool IsEmpty()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Размер коллекции
        /// </summary>
        public int Size()
        {
            throw new NotImplementedException();
        }
    }
}
