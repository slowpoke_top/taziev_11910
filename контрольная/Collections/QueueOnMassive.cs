﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collections
{
    /// <summary>
    /// Очередь на основе массива - 3
    /// </summary>
    public class QueueOnMassive<T>
    {
        private T[] elements;

        /// <summary>
        /// Добавление
        /// </summary>
        public void Push(T item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаление
        /// </summary>
        public T Pop()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        public bool IsEmpty()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Размер коллекции
        /// </summary>
        public int Size()
        {
            throw new NotImplementedException();
        }
    }
}
