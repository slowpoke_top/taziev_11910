﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Collections
{
    /// <summary>
    /// Дек на основе Листа - 7
    /// </summary>
    public class DeqOnList<T>
    {
        private Node<T> head;

        /// <summary>
        /// Добавление в конец
        /// </summary>
        public void PushBack(T item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаление с конца
        /// </summary>
        public T PopBack()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Добавление в начало
        /// </summary>
        public void PushFront(T item)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Удаление с начала
        /// </summary>
        public T PopFront()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Проверка на пустоту
        /// </summary>
        public bool IsEmpty()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Размер коллекции
        /// </summary>
        public int Size()
        {
            throw new NotImplementedException();
        }
    }
}
