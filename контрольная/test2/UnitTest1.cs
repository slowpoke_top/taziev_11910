﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Collections;
namespace Test
{
    [TestClass]
    public class CollectionTest
    {
        [TestMethod]
        public void TestAdding()
        {
            QueueOnTwoList<int> a = new QueueOnTwoList<int>();
            a.Push(12);
            a.Push(12);
            a.Push(12);
            Assert.AreEqual(a.Size(), 3);
        }

        [TestMethod]
        public void TestDeleting()
        {
            QueueOnTwoList<int> a = new QueueOnTwoList<int>();
            a.Push(12);
            a.Push(233);
            int s = a.Pop();
            Assert.AreEqual(233, a.Pop());
        }

        [TestMethod]
        public void TestIsEmpty()
        {
            QueueOnTwoList<int> a = new QueueOnTwoList<int>();
            Assert.AreEqual(a.IsEmpty(), true);
            a.Push(11);
            Assert.AreEqual(a.IsEmpty(), false);
        }

        [TestMethod]
        public void TestSize()
        {
            QueueOnTwoList<int> a = new QueueOnTwoList<int>();
            Assert.AreEqual(a.Size(), 0);
            a.Push(11);
            Assert.AreEqual(a.Size(), 1);
        }
    }
}
