﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /// <summary>
    /// Коллекция на основе массива
    /// </summary>
    public class ArrayList<T> : ICustomCollection<T>
        where T : IComparable<T>
    {
        private T[] array;
        private int size;

        ///<inheritdoc/>
        public void Add(T elem)
        {
            //добавление в пустой список
            if (isEmpty()) {
                array = new T[1] { elem };
                size = 1;
                return;
            }
            //Список не пустой, место есть
            if (array.Length > size)
            {
                array[size] = elem;
                size += 1; 
                return;
            }
            else 
            {
                var newArray = new T[array.Length * 2];
                for (int i = 0; i < array.Length; i++)
                {
                    newArray[i] = array[i];
                }

                newArray[size] = elem;
                size++;
                array = newArray;
            }
        }

        ///<inheritdoc/>
        public void AddRange(T[] elems)
        {
            throw new NotImplementedException();
        }

        public void Clear()
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public bool Contains(T elem)
        {
            int count = 0;
            while (size != count)
            {
                if (array[count].CompareTo(elem) == 0) return true;
                count++;
            }

            return false;
        }

        ///<inheritdoc/>
        public int IndexOf(T elem)
        {
            int count = 0;
            while (size != count)
            {
                if (array[count].CompareTo(elem) == 0) return count;
                count++;
            }

            return -1;
        }

        ///<inheritdoc/>
        public void Insert(int index, T elem)
        {
           var Dark = new T[array.Length * 2];
           var a = 0;
           for (int i = 0; i < size; i++)
           {
               if (i == index)
               {
                   Dark[i] = elem;
                   a = i;
                   break;
               }
               else
               {
                   Dark[i] = array[i];
               }
           }

           for (int i = a; i < size; i++)
           {
               Dark[i + 1] = array[i];
           }

           array = new T[array.Length*2];
           array = Dark;
           size++;
        }

        public bool isEmpty()
        {
            return size == 0;
        }

        ///<inheritdoc/>
        public void Remove(T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void RemoveAll(T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Reverse()
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public int Size()
        {
            return size;
        }
        public override string ToString()
        {
            if (array == null) return null;
            var node = array;
            var sb = new StringBuilder();
            int i = 0;
            while (i != size)
            {
                sb.Append(" " + node[i].ToString());
                i++;
            }
            return sb.ToString();
        }
    }
}