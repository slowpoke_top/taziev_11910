﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CustomCollection
{
    public class CustomList<T> : ICustomCollection<T> where T: IComparable<T>
    {
        /// <summary>
        /// Головной элемент
        /// </summary>
        private Node<T> head;

        ///<inheritdoc/>
        public void Add(T elem)
        {
            if (head == null)
                head = new Node<T>() { Data = elem };
            else {
                var node = head;
                while (node.NextNode != null)
                    node = node.NextNode;
                node.NextNode = new Node<T>() { Data = elem };
            }
        }
        ///<inheritdoc/>
        public void AddRange(T[] elems)
        {
            var arr = elems;
            var node = head;
            
            while (node.NextNode != null)
                node = node.NextNode;
            for (int i = 0; i < arr.Length; i++)
            {
                if (head == null)
                    head = new Node<T>() { Data = elems[i] };
                else {
                    node.NextNode = new Node<T>() { Data = elems[i] };
                }
            }
        }

        public void Clear()
        {
            head.Data = default(T);
            head.NextNode = null;
        }
        ///<inheritdoc/>
        public bool Contains(T elem)
        {
            var node = head;
            do
            {
                if (node.Data.CompareTo(elem) == 0)
                {
                    return true;
                }
                node = node.NextNode;
            }while(node != null);

            return false;
        }
        ///<inheritdoc/>
        public int IndexOf(T elem)
        {
            if (head == null) return -1;
            int a = 1;
            var node = head;
            var node1 = head.NextNode;
            while (true)
            {
                node = node.NextNode;
                node1 = node1.NextNode;
                ++a;
                if(node1.Data.CompareTo(elem) == 0)  return a;
                if (a == Size()) return -1;
            }
        }
        ///<inheritdoc/>
        public void Insert(int index, T elem)
        {
            int a = 1;
            var node = head;
            var node1 = head.NextNode;
            while (a != index)
            {
                node = node.NextNode;
                node1 = node1.NextNode;
                a++;
            }
            node.NextNode = new Node<T>();
            node = node.NextNode;
            node.Data = elem;
            node.NextNode = node1;
        }
        ///<inheritdoc/>
        public bool isEmpty()
        {
            return head == null;
        }
        ///<inheritdoc/>
        public void Remove(T elem)
        {
            if (isEmpty()) return;
            var node = head;
            var node1 = head.NextNode;
            if (head.NextNode == null)
            {
                head = null;
                return;
            }
            while (node1 != null) {
                if (node1.Data.CompareTo(elem) != 0)
                {
                    node = node.NextNode;
                    node1 = node1.NextNode;
                }
                else
                {
                    node.NextNode = node1.NextNode;
                    return;
                    
                }
            }
        }
        ///<inheritdoc/>
        public void RemoveAll(T elem)
        {
            if (isEmpty()) return;
            var node = head;
            Node<T> prevNode = null;
            while (node != null) {
                if (node.Data.CompareTo(elem) != 0)
                {
                    prevNode = node;
                    node = node.NextNode;
                }
                else {
                    if (prevNode == null) //головной элемент
                    {
                        head = head.NextNode;
                        node = head.NextNode;
                    }
                    else
                    {
                        prevNode.NextNode = node.NextNode;
                        node = node.NextNode;
                    }
                }
            }
        }
        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            int a = 1;
            var node = head;
            var node1 = head.NextNode;
            while (a != index)
            {
                node = node.NextNode;
                node1 = node1.NextNode;
                a++;
            }

            node.NextNode = node1.NextNode;
 
        }
        ///<inheritdoc/>
        public void Reverse()
        {
            Node<T> node;
            if (isEmpty()) return;
            node = new Node<T>() { Data = head.Data };
            while (head != null) 
            {  
                head = head.NextNode;
                if (isEmpty())
                {
                    head = node;
                    return;
                }
                node = new Node<T>() { Data = head.Data, NextNode = node };
            }
        }
        ///<inheritdoc/>
        public int Size()
        {
            if (head == null) return 0;
            var node = head;
            int a = 1;
            while (node.NextNode != null)
            {
                ++a;
                node = node.NextNode;
            }

            return a;
        }

        /// <summary>
        /// Вывести список строкой
        /// </summary>
        /// <returns></returns>
        public override string ToString() {
            if (head == null) return null;
            var node = head;
            var sb = new StringBuilder();
            while (node != null) {
                sb.Append(" " + node.Data.ToString());
                node = node.NextNode;
            }
            return sb.ToString();
        }
    }
}
