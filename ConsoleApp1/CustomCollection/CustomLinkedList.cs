﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /// <summary>
    /// Двунаправленный список
    /// </summary>
    public class CustomLinkedList<T> : ICustomCollection<T> where T : IComparable<T>
    {
        private LinkedNode<T> head;

        ///<inheritdoc/>
        public void Add(T elem)
        {
            if (head == null)
                head = new LinkedNode<T>() { Data = elem };
            else
            {
                var node = head;
                while (node.NextNode != null) {
                    node = node.NextNode;
                }
                var newNode = new LinkedNode<T>() { Data = elem };
                node.NextNode = newNode;
                newNode.PrevNode = node;
            }
        }

        ///<inheritdoc/>
        public void AddRange(T[] elems)
        {
            var arr = elems;
            var node = head;
            while (node.NextNode != null) node = node.NextNode;
            for(int i = 0; i < elems.Length;i++)
            {
                if (head == null)
                    head = new LinkedNode<T>() {Data = arr[i]};
                else
                {
                    

                    var newNode
                        = new LinkedNode<T>() {Data = arr[i]};
                    node.NextNode = newNode;
                    newNode.PrevNode = node;
                }
            }
        }

        ///<inheritdoc/>
        public void Clear()
        {
            head.Data = default(T);
            head.NextNode = null;
        }

        ///<inheritdoc/>
        public bool Contains(T elem)
        {
            var node = head;
            while (node.NextNode != null)
            {
                if (node.Data.CompareTo(elem) == 0)
                {
                    return true;
                }
                node = node.NextNode;
            }

            return false;
            
        }

        ///<inheritdoc/>
        public int IndexOf(T elem)
        {
            var node = head;
            int a = 0;
            while (node.NextNode != null)
            {
                if (node.Data.CompareTo(elem) == 0)
                {
                    return a;
                }
                a++;
                node = node.NextNode;
            }

            return -1;
        }

        ///<inheritdoc/>
        public void Insert(int index, T elem)
        {
            int a = 1;
            var node = head;
            var node1 = head.NextNode;
            while (a != index)
            {
                node = node.NextNode;
                node1 = node1.NextNode;
                a++;
            }
            node.NextNode = new LinkedNode<T>();
            node.NextNode.PrevNode = node;
            node = node.NextNode;
            node.Data = elem;
            node.NextNode = node1;
            node1.PrevNode = node;
            
        }

        ///<inheritdoc/>
        public bool isEmpty()
        {
            if (head.Data == null) return true;
            else return false;
        }

        ///<inheritdoc/>
        public void Remove(T elem)
        {
            if (isEmpty()) return;
            if (head.Data.CompareTo(elem) == 0)
            {
                head = head.NextNode;
                head.PrevNode = null;
            }
            int a = Size();
            var node = head;
            var node1 = head.NextNode;
            while (node1 != null) {
                if (node1.Data.CompareTo(elem) != 0)
                {
                    node = node.NextNode;
                    node1 = node1.NextNode;
                    a--;
                }
                else
                {
                    if (a == 2)
                    {
                        node.NextNode = null;
                        return;
                    }
                    node.NextNode = node1.NextNode;
                    node1.NextNode.PrevNode = node;
                    return;
                    
                }
            }
        }

        ///<inheritdoc/>
        public void RemoveAll(T elem)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void RemoveAt(int index)
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public void Reverse()
        {
            throw new NotImplementedException();
        }

        ///<inheritdoc/>
        public int Size()
        {
            if (head == null) return 0;
            var node = head;
            int a = 1;
            while (node.NextNode != null)
            {
                ++a;
                node = node.NextNode;
            }
            return a;
        }

        /// <summary>
        /// Вывести список строкой
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (head == null) return null;
            var node = head;
            var sb = new StringBuilder();
            while (node != null)
            {
                sb.Append(" " + node.Data.ToString());
                node = node.NextNode;
            }
            return sb.ToString();
        }
    }
}
