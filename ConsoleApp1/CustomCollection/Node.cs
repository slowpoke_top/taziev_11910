﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomCollection
{
    /// <summary>
    /// Элемент линейного списка
    /// </summary>
    public class Node<T> 
    {
        /// <summary>
        /// Информационное поле
        /// </summary>
        public T Data { get; set; }

        /// <summary>
        /// Ссылка на следующий элемент
        /// </summary>
        public Node<T> NextNode { get; set; }
    }
}
