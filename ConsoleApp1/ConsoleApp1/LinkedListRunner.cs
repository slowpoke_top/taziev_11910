﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomCollection;

namespace ConsoleApp1
{
    /// <summary>
    /// Примеры для двунаправленного связного списка
    /// </summary>
    public class LinkedListRunner
    {
        public void Run() {
            var list = new CustomLinkedList<int>();
            list.AddRange(new[] {4, 6, 0, 34, 14, 11, 14, 777});
            Console.WriteLine(list.Size());
            Console.WriteLine(list.ToString());
            list.Insert(3, 9999999);
            list.Remove(777);
            Console.WriteLine(list.Size());
            Console.WriteLine(list.ToString());
        }
    }
}
