﻿using System;
using System.Collections.Generic;
using System.Text;
using CustomCollection;

namespace ConsoleApp1
{
    public class ArrayListRunner
    {
        public void Run() {
            var list = new ArrayList<int>();
            list.Add(3);
            Console.WriteLine(list.ToString());
            list.Add(4);
            list.Add(5);
            list.Add(6);
            list.Insert(2,999);
            Console.WriteLine(list.ToString());
        }
    }
}