﻿using System;

namespace Mchs
{
    public class Stantion
    {
        public static event EventHandler<VacationParamsEventArgs> GoOnVacationEvent;
        
        public void GoOnVacation(ref VacationParamsEventArgs param)
        {
            if (GoOnVacationEvent != null)
                param.timer++;
                GoOnVacationEvent(this, param);
        }
    }

    public class VacationParamsEventArgs : EventArgs
    {
        public int count = 0;
        public int Humans = 1000;
        public int HumansInFire = 0;
        public int HumansWasSaved = 0;
        public int FireDetecor = 0;
        public long timer = 0;
        public int critFire = 200;
        public int verycritFire = 1000;
        public int thisFire;
        public Random rand = new Random();

    }
}