﻿using System;
using System.Timers;

namespace Mchs
{
    public static class Nursery
    {
        public static void Register()
        {
            Stantion.GoOnVacationEvent += NurseryGoing;
        }

        public static void FireDeleter(Stantion f,VacationParamsEventArgs a)
        {
            a.timer = 0;
            a.thisFire = 0;
            while (true)
            {
                f.GoOnVacation(ref a);
            }
        }

        private static void NurseryGoing(object sender, VacationParamsEventArgs a)
        {
            var send = sender as Stantion;
            if(a.thisFire < a.critFire)Console.WriteLine("Пока, что все хорошо, продолжаем наблюдать;)");
            else if (a.thisFire == a.critFire)
            {
                Console.WriteLine("Мы выехали, надеюсь успеем");
            }
            else
            {
                Console.WriteLine("БЛИН НЕ УСПЕЛИ, ВСЕ СГОРЕЛО");
                
                
            }
        }
    }
}