﻿using System;
using System.Diagnostics.Eventing.Reader;
using System.Threading;
using System.Timers;

namespace Mchs
{
    public static class Firetruck
    {
        public static void Register()
        {
            Stantion.GoOnVacationEvent += FireDelete;
        }

        public static void FireDeleter(Stantion f,VacationParamsEventArgs a)
        {
            a.timer = 0;
            a.thisFire = 0;
            if (a.count == 1)
            {
                a.FireDetecor = 0;
            }

            while (true)
            {
                f.GoOnVacation(ref a);
            }
        }

        private static void FireDelete(object sender, VacationParamsEventArgs a)
        {
            var send = sender as Stantion;
            a.thisFire += a.rand.Next(0, 10);
            if (a.thisFire < a.critFire && !(a.FireDetecor != 0))
            {
                Console.WriteLine("Пока, что все хорошо, значение жары = " + a.thisFire);
            }
            else if (a.thisFire >= a.critFire && !(a.FireDetecor != 0))
            {
                Console.WriteLine("Мы выехали, надеюсь успеем, значение огня = " + a.thisFire);
                a.FireDetecor = 1;
            }
            else if (a.thisFire > a.critFire && a.thisFire < a.verycritFire || (a.FireDetecor == 1))
            {
                Thread.Sleep(20);
                Console.WriteLine("МЫ ТУШИМ ТУШИМ значение огня = " + a.thisFire );
                a.thisFire -= 10;
                if (a.thisFire <= 0)
                {
                    a.count = 1;
                    Console.WriteLine("FIRE ПОБЕЖДЕН");
                    while (true)
                    {
                        Console.WriteLine("Чтобы начать заново нажмите R");
                        if (Console.ReadKey(true).Key == ConsoleKey.R)
                        {
                            FireDeleter(send, a);
                        }
                    }
                }
                else 
                {
                    a.count = 1;
                    Console.WriteLine("БЛИН НЕ УСПЕЛИ ПОТУШИТЬ, ВСЕ СГОРЕЛО" + " значение огня = " + a.thisFire);
                    while (true)
                    {
                        Console.WriteLine("Чтобы начать заново нажмите R");
                        if (Console.ReadKey(true).Key == ConsoleKey.R)
                        {
                            FireDeleter(send,a);
                        }
                    }
                }
            }
        }
    }
}