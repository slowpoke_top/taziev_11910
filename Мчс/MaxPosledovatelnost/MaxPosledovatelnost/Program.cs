﻿using System;
using System.Linq;
using System.Text;

namespace MaxPosledovatelnost
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Введите в строчку массив цифр");
            int[] a = Console.ReadLine().Split(' ').Where(x => x!=" " ).Select(x => Int32.Parse(x)).ToArray();
            int[] l = new int[a.Length];
            int[] n = new int[a.Length];
            l[0] = 1;
            n[0] = 0;
            int maxl = -1;
            int maxli = -1;
            for (int i = 1; i < a.Length; i++)
            {
                for (int j = i - 1; j > -1; j--)
                {
                    if (a[i] > a[j])
                    {
                        if (maxl < l[j])
                        {
                            maxl = l[j];
                            maxli = j + 1;
                        }
                    }
                }
                if (maxl == -1)
                {
                    l[i] = 1;
                    n[i] = 0;
                }
                else
                {
                    l[i] = maxl + 1;
                    n[i] = maxli;
                }
                maxl = -1;
            }
            int index = Array.FindLastIndex(l, delegate(int i) { return i == l.Max(); });
            var str = new StringBuilder();
            for (int i = 0; i < l.Max(); i++)
            {
                if (i != l.Max() - 1)
                    str.Append(a[index] + " <- ");
                else str.Append(a[index]);
                if (index - 1 > 0)
                    index = n[index - 1];
                else index = 0;
            }
            Console.WriteLine(" Max = " + str.ToString());
        }
    }
}